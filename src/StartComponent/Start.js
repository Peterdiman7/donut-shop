import Donut from "../DonutComponent/Donut";
import styles from "../StartComponent/Start.module.css";
import Title from "../TitleComponent/Title";
import { Link } from "react-router-dom";

const Start = () => {

    return(
        <>
        <div className={styles.container}>
            <span className={styles.polygon}>
            </span>
            <button className={styles.button}>
            </button>
            <Link to={"/menu"}>
                <p className={styles.buttonText}>Start</p>
            </Link>
            <Title variant="title" name={["The", "Donut Shop"]} />
            <Donut variant="title" />
        </div>
        </>
    );
}

export default Start;

