import "./App.css";
import { Route, Routes } from "react-router-dom";
import Start from "./StartComponent/Start";
import Menu from "./MenuComponent/Menu";
import Payment from "./PaymentComponent/Payment";
import Preparing from "./PreparingComponent/Preparing";
import Ready from "./ReadyComponent/Ready";

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Start />} />
        <Route path="/menu" element={<Menu />} />
        <Route path="/payment" element={<Payment />} />
        <Route path="/preparing" element={<Preparing />} />
        <Route path="/ready" element={<Ready />} />
      </Routes>
    </div>
  );
}

export default App;
