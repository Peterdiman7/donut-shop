import Donut from "../DonutComponent/Donut";
import styles from "../ReadyComponent/Ready.module.css";
import Title from "../TitleComponent/Title";

import { Link } from "react-router-dom";

const Ready = () => {
    return(
        <>
        <div className={styles.container}>
            <span className={styles.polygon}></span>
                <Donut variant="ready" />
                <Title variant="ready" name={["You can now come and pick up your amazing Donut!"]} />
                <Link to={"/"}>
                <span className={styles.button}></span>
                    <p className={styles.text}>Done</p>
                </Link>
        </div>
        </>
    );
}

export default Ready;