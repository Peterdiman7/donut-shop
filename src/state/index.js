import { createGlobalState } from "react-hooks-global-state";

const {setGlobalState, useGlobalState} = createGlobalState({
    defaultDonut: "trueBlood"
});

export {useGlobalState, setGlobalState};