import { useEffect } from "react";
import { redirect, useNavigate } from "react-router-dom";
import Donut from "../DonutComponent/Donut";
import styles from "../PreparingComponent/Preparing.module.css";
import Title from "../TitleComponent/Title";

const Preparing = () => {
    const navigate = useNavigate();
    useEffect(() => {
        setTimeout(() => {
            navigate('/ready')
        }, 5000);
    });
    return(
        <div className={styles.container}>
            <Donut variant="preparing" />
            <Title variant="preparing" name={["Preparing..."]} />
        </div>
    );
}

export default Preparing;