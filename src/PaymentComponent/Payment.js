import Donut from "../DonutComponent/Donut";
import styles from "../PaymentComponent/Payment.module.css";
import Title from "../TitleComponent/Title";

import { Link } from "react-router-dom";

import applePay from "../images/apple-pay.png"
import googlePay from "../images/google-pay.png";

const Payment = () => {
    return(
        <div className={styles.container}>
            <Title variant="payment" name={["You can", "Pay us", "NOW"]} />
            <Donut variant="payment" />
            <Link to={"/preparing"}>
            <span className={styles.applePay}>
                <img className={styles.applePayLogo} src={applePay} alt="applePayImg" />
            </span>
            </Link>
            <Link to={"/preparing"}>
            <span className={styles.googlePay}>
                <img className={styles.googlePayLogo} src={googlePay} alt="" />
            </span>
            </Link>
        </div>
    );
}

export default Payment;