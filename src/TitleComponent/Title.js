import styles from "../StartComponent/Start.module.css";
import menuStyles from "../MenuComponent/Menu.module.css";
import paymentStyles from "../PaymentComponent/Payment.module.css";
import preparingStyles from "../PreparingComponent/Preparing.module.css";
import readyStyles from "../ReadyComponent/Ready.module.css";

const Title = (props) => {
if(props.variant == 'menu'){
    return(
        <>
        <div>
            <h1 className={menuStyles.menuTitleOne}>{props.name[0]}</h1>
            <h1 className={menuStyles.menuTitleTwo}>{props.name[1]}</h1>
        </div>
        <div>
            <h1 className={menuStyles.skyShapedTitle}>{props.donut[0]}</h1>
            <h1 className={menuStyles.marbleMagicTitle}>{props.donut[1]}</h1>
            <h1 className={menuStyles.trueBloodTitle}>{props.donut[2]}</h1>
            <h1 className={menuStyles.unicornDustTitle}>{props.donut[3]}</h1>
        </div>
        </>
    );
}else if(props.variant == 'title') {
    return(
        <div>
            <h1 className={styles.propsOne}>{props.name[0]}</h1>
            <h1 className={styles.propsTwo}>{props.name[1]}</h1>
            <h1 className={styles.propsTwo}>{props.name[2]}</h1>
        </div>
    );
}else if(props.variant == 'payment') {
    return(
        <div>
            <h1 className={paymentStyles.titleOne}>{props.name[0]}</h1>
            <h1 className={paymentStyles.titleTwo}>{props.name[1]}</h1>
            <h1 className={paymentStyles.titleThree}>{props.name[2]}</h1>
        </div>
    );
}else if(props.variant == 'preparing') {
    return(
        <div>
            <h1 className={preparingStyles.title}>{props.name[0]}</h1>
        </div>
    );
}else if(props.variant == 'ready') {
    return(
        <div>
            <h1 className={readyStyles.title}>{props.name[0]}</h1>
        </div>
    );
}
}

export default Title;