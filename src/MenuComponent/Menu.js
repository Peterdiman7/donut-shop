import { useState } from "react";
import Donut from "../DonutComponent/Donut";
import styles from "../MenuComponent/Menu.module.css";
import Title from "../TitleComponent/Title";

const Menu = () => {
    const [donutType, setDonutType] = useState();
    return(
        <div className={styles.container}>
            <Title variant="menu" name={["The", "MENU"]} donut={["Sky Shaped", "Marble Magic", "True Magic", "Unicorn Magic"]}/>
            <Donut variant="menu" setDonutType={setDonutType} />
        </div>
    );
}

export default Menu;