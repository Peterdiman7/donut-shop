import { setGlobalState, useGlobalState } from "../state";
import styles from "../DonutComponent/Donut.module.css";
import paymentStyles from "../PaymentComponent/Payment.module.css";
import preparingStyles from "../PreparingComponent/Preparing.module.css";
import readyStyles from "../ReadyComponent/Ready.module.css";
import menuStyles from "../MenuComponent/Menu.module.css";

import { Link } from "react-router-dom";

import unicornDust from "../images/unicorn_dust.svg"
import sparkles from "../images/sparkles.svg";
import trueBlood from "../images/true_blood.svg";
import skyShaped from "../images/sky_shaped.svg";
import marbleMagic from "../images/marble_magic.svg";

const Donut = (props) => {
    const [defaultDonut] = useGlobalState("defaultDonut");
        const handleDonut = (e) => {
            setGlobalState("defaultDonut", e.target.alt);
        }
        console.log(defaultDonut);

    console.log(defaultDonut);
    if(props.variant === 'title'){
        return(
        <h1>
            <img className={styles.unicornDust} src={unicornDust} alt="image" />
            <img className={styles.sparkles} src={sparkles} alt="image" />
            <img className={styles.trueBlood} src={trueBlood} alt="image" />
        </h1>
    );
    
    }else if(props.variant === 'menu'){
        return(
        <h1>
            <Link to={"/payment"}>
            <img onClick={handleDonut} className={menuStyles.skyShaped} src={skyShaped} alt="sky_shaped" />
            </Link>
            <Link to={"/payment"}>
            <img onClick={handleDonut} className={menuStyles.marbleMagic} src={marbleMagic} alt="marble_magic"/>
            </Link>
            <Link to={"/payment"}>
            <img onClick={handleDonut} className={menuStyles.trueBlood} src={trueBlood} alt="true_blood" />
            </Link>
            <Link to={"/payment"}>
            <img onClick={handleDonut} className={menuStyles.unicornDust} src={unicornDust} alt="unicorn_dust" />
            </Link>
        </h1>
    );
    
}else if(props.variant === 'payment'){
        return(
        <h1>
            <img className={paymentStyles.skyShaped} src={skyShaped} alt="image" />
            <img className={paymentStyles.unicornDust} src={unicornDust} alt="image" />
        </h1>
    );
}else if(props.variant === 'preparing'){
        return(
        <div>
            <img className={preparingStyles.sparkles} src={sparkles} alt="image" />
            <img className={preparingStyles.trueBlood} src={defaultDonut === "sky_shaped" ? "/static/media/sky_shaped.9342426b.svg" : ''
                                        || defaultDonut === "marble_magic" ? "/static/media/marble_magic.4376f996.svg" : ''
                                        || defaultDonut === "true_blood" ? "/static/media/true_blood.e3be7763.svg" : ''
                                        || defaultDonut === "unicorn_dust" ? "/static/media/unicorn_dust.9b5cbb37.svg" : ''}
                                        alt="image" />
        </div>
    );
}else if(props.variant === 'ready'){
        return(
        <div>
            <img className={readyStyles.trueBlood} src={defaultDonut === "sky_shaped" ? "/static/media/sky_shaped.9342426b.svg" : ''
                                        || defaultDonut === "marble_magic" ? "/static/media/marble_magic.4376f996.svg" : ''
                                        || defaultDonut === "true_blood" ? "/static/media/true_blood.e3be7763.svg" : ''
                                        || defaultDonut === "unicorn_dust" ? "/static/media/unicorn_dust.9b5cbb37.svg" : ''}
                                        alt="image" />
        </div>
    );
}
}

export default Donut;